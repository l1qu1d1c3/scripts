# Requires JQ and PBCOPY
# Requires vault to be installed in /usr/local/bin
args=$1

IFS=' ' read -r -a array <<< "$args"
export VAULT_ADDR=
export VAULT_API_ADDR=
export VAULT_TOKEN=

/usr/local/bin/vault kv get --format=json ${array[0]}/$(echo ${array[1]} | tr a-z A-Z) | /usr/local/bin/jq .data.data | pbcopy
